/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.clustering;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.ucn.disc.biblio.refcluster.reference.Author;
import cl.ucn.disc.biblio.refcluster.reference.Reference;
import cl.ucn.disc.biblio.refcluster.reference.ReferenceManager;

/**
 *
 * @author Jaime Pavlich-Mariscal
 */
public class ClustererTest {

    
    

    public ClustererTest() {
		Properties logProperties = new Properties();
		logProperties.put("log4j.rootLogger", "INFO, CONSOLE");
		logProperties.put("log4j.appender.CONSOLE", "org.apache.log4j.ConsoleAppender"); // A standard console appender
		logProperties.put("log4j.appender.CONSOLE.layout", "org.apache.log4j.PatternLayout"); //See: http://logging.apache.org/log4j/docs/api/org/apache/log4j/PatternLayout.html
		logProperties.put("log4j.appender.CONSOLE.layout.ConversionPattern", "%d{yyyy-MM-dd HH:mm:ss} %c{1} [%p]   %m%n");
		
		PropertyConfigurator.configure(logProperties);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }


    /** Test cluster file creation and reading
     * @throws Exception 
     */
    @Test
    public void testReadWriteClusters() throws Exception {
        ReferenceManager rm = new ReferenceManager();
        ClusterManager cm = new ClusterManager();
        Set<Reference> refs = rm.readRefsFromPlainTextFile(new File("testData/testRefs.txt"));
        
        Set<Set<Reference>> clusters = cm.clusterReferences(refs);
        cm.writeReferenceClusters(clusters, new PrintWriter(new File("testData/clusterSaveTest.txt")));

        Set<Set<Reference>> clustersRead = cm.readClusters(new File("testData/clusterSaveTest.txt"), Reference.class);

        cm.writeReferenceClusters(clustersRead, new PrintWriter(new File("testData/clusterSaveTest2.txt")));



        assertEquals(clusters, clustersRead);
    }
    
    @Test
    public void testAuthorCluster() throws Exception {
    	ReferenceManager rm = new ReferenceManager();
    	ClusterManager cm = new ClusterManager();
    	
    	Set<Reference> refs = rm.readRefsFromPlainTextFile(new File("testData/ReferenceClustererTest/authorClusterTest/references.txt"));
    	Set<Set<Author>> clusters = cm.clusterAuthorsFromReferences(refs);
    	Set<Set<Author>> expectedClusters = cm.readClusters(new File("testData/ReferenceClustererTest/authorClusterTest/expectedAuthorClusters.txt"), Author.class);
    	
    	
		Comparator<Set<Author>> firstElementComparator = new Comparator<Set<Author>>() {

			
			@Override
			public int compare(Set<Author> o1, Set<Author> o2) {
				if (o1.size() > 0 && o2.size() > 0) {
					return o1.iterator().next().compareTo(o2.iterator().next());
				} else {
					return o1.size() > o2.size()? -1 : o1.size() <o2.size()? 1 : 0;
				}
			}

		};
    	
//    	cm.writeClusters(clusters, new PrintWriter("a.txt"), firstElementComparator);
//    	cm.writeClusters(expectedClusters, new PrintWriter("b.txt"), firstElementComparator);
    	
    	assertTrue(clusters.equals(expectedClusters));

    }

}
