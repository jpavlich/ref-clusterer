/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.reference;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.ucn.disc.biblio.refcluster.clustering.ClusterManager;

/**
 *
 * @author Jaime Pavlich-Mariscal
 */
public class ReferenceManagerTest {


    public ReferenceManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private void compareReferenceLists(Collection<Reference> refs1, Collection<Reference> refs2) {
        assertTrue(refs1.size() == refs2.size());
        for (Reference r : refs1) {
            assertTrue(refs2.remove(r));
        }
        assertTrue(refs2.isEmpty());
    }

    /** Verifies that reading of references from a WoS file
     * yields the same results as a pre-generated list of references
     * that we know, in advance, that is correct result.
     * @throws FileNotFoundException 
     */
    @Test
    public void testReadRefsFromWoSFile() throws FileNotFoundException {
        ReferenceManager rm = new ReferenceManager();
        Collection<Reference> refs1 = rm.readRefsFromPlainTextFile(new File("testData/ReferenceManagerTest/testReadRefsFromWoSFileCorrect.txt"));
        Collection<Reference> refs2 = rm.readRefsFromWoSFile(new File("testData/ReferenceManagerTest/testDB/data1.txt"));

        Comparator<Reference> comparator = new Comparator<Reference>() {

			@Override
			public int compare(Reference o1, Reference o2) {
				return o1.getString().compareTo(o2.getString());
			}
		};
        
        compareReferenceLists(refs1, refs2);
    }

    /** Verifies that reading of references from a folder with WoS files
     * yields the same results as a pre-generated list of references
     * that we know, in advance, that is correct result.
     * @throws IOException 
     */
    @Test
    public void testReadRefsFromWoSFolder() throws IOException {
        ReferenceManager rm = new ReferenceManager();
        Collection<Reference> refs1 = rm.readRefsFromPlainTextFile(new File("testData/ReferenceManagerTest/testReadRefsFromWoSFolderCorrect.txt"));
        Collection<Reference> refs2 = rm.readRefsFromWoSFolder(new File("testData/ReferenceManagerTest/testDB"));
//        for (Reference r : refs2) {
//        	System.out.println(r);
//        }
        compareReferenceLists(refs1, refs2);
    }

    @Test
    public void testAuthorReplacement() throws Exception {
    	ClusterManager cm = new ClusterManager();
    	ReferenceManager rm = new ReferenceManager();
    	
    	Collection<Set<Author>> clusters = cm.readClusters(new File("testData/ReferenceManagerTest/authorReplacementTest/author_clusters.txt"), Author.class);
    	Map<Author,String> authorReplacements = cm.getAuthorReplacementsFromClusters(clusters);
    	rm.replaceAuthorsOfWoSFolder(new File("testData/ReferenceManagerTest/testDB-src"), new File("testData/ReferenceManagerTest/testDB-dest"), authorReplacements);
    	
    	assertTrue("Test incomplete", false);
    }
    
    @Test
    public void testUnion() throws Exception {
    	ReferenceManager rm = new ReferenceManager();
    	
    	Map<String,BiblioRecord> map1 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data1.txt");
    	Map<String,BiblioRecord> map2 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data2.txt");
    	
    	rm.union("testData/ReferenceManagerTest/testDB/union.txt", "testData/ReferenceManagerTest/testDB/data1.txt", "testData/ReferenceManagerTest/testDB/data2.txt");
    	Map<String,BiblioRecord> union = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/union.txt");
    	
    	Set<String> keysets = new HashSet<String>();
    	keysets.addAll(map1.keySet());
    	keysets.addAll(map2.keySet());
    	
    	assertTrue(!union.isEmpty());
    	assertEquals(union.keySet(), keysets);
    	
    }
    
    @Test
    public void testIntersection() throws Exception {
    	ReferenceManager rm = new ReferenceManager();
    	
    	Map<String,BiblioRecord> map1 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data1.txt");
    	Map<String,BiblioRecord> map2 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data2.txt");
    	
    	rm.intersection("testData/ReferenceManagerTest/testDB/intersection.txt", "testData/ReferenceManagerTest/testDB/data1.txt", "testData/ReferenceManagerTest/testDB/data2.txt");
    	Map<String,BiblioRecord> intersection = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/intersection.txt");
    	
    	Set<String> keysets = new HashSet<String>();
    	keysets.addAll(map1.keySet());
    	keysets.retainAll(map2.keySet());
    	
    	assertTrue(!intersection.isEmpty());
    	assertEquals(intersection.keySet(), keysets);
    	
    }

    
    @Test
    public void testDifference() throws Exception {
    	ReferenceManager rm = new ReferenceManager();
    	
    	Map<String,BiblioRecord> map1 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data1.txt");
    	Map<String,BiblioRecord> map2 = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/data2.txt");
    	
    	rm.difference("testData/ReferenceManagerTest/testDB/difference.txt", "testData/ReferenceManagerTest/testDB/data1.txt", "testData/ReferenceManagerTest/testDB/data2.txt");
    	Map<String,BiblioRecord> difference = BiblioRecord.getMap("testData/ReferenceManagerTest/testDB/difference.txt");
    	
    	Set<String> keysets = new HashSet<String>();
    	keysets.addAll(map1.keySet());
    	keysets.removeAll(map2.keySet());
    	
    	assertTrue(!difference.isEmpty());
    	assertEquals(difference.keySet(), keysets);
    	
    }
}
