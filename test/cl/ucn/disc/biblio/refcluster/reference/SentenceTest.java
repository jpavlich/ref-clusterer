/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.ucn.disc.biblio.refcluster.database.TupleOfStrings;
import cl.ucn.disc.biblio.refcluster.reference.Sentence;

/**
 *
 * @author Jaime Pavlich-Mariscal
 */
public class SentenceTest {

    @SuppressWarnings("unused")
	private TupleOfStrings s2;
    @SuppressWarnings("unused")
	private TupleOfStrings s3;
    @SuppressWarnings("unused")
	private TupleOfStrings s4;
    private Sentence s5;
    private Sentence s1;
    private Sentence sm1;
    private Sentence sm2;
    private Sentence sm3;
    private Sentence sm4;
    private Sentence sm5;
    private Sentence sm6;
    private Sentence sm7;
    private Sentence sm8;
    private Sentence s6;

    public SentenceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        s1 = new Sentence();
        s2 = new Sentence();
        s3 = new Sentence("a aab d ");
        s4 = new Sentence("a af31 d ");
        s5 = new Sentence("a b c");
        s6 = new Sentence("a b c");

        sm1 = new Sentence("IEE COMPUT");
        sm2 = new Sentence("IEEE COMPUTER");
        sm3 = new Sentence("IEE COMPARE");
        sm4 = new Sentence("IEEE COMP");
        sm5 = new Sentence("IEE COMPUTING");
        sm6 = new Sentence("COMMUNIC CO COMPA");
        sm7 = new Sentence("COM COMP COMPARE");
        sm8 = new Sentence("COMM COMP");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of initialize method, of class Sentence.
     */
    @Test
    public void testInitialize_String() {
        String str1 = "hola mundo chao*";
        s1.parse(str1);
        assertEquals(s1.string, str1);
        assertNotNull(s1.wordsMultiMap.get("hola"));
        assertNotNull(s1.wordsMultiMap.get("mundo"));
        assertNotNull(s1.wordsMultiMap.get("chao"));

        assertNotNull(s1.prefixTree.contains("hola"));
        assertNotNull(s1.prefixTree.contains("mundo"));
        assertNotNull(s1.prefixTree.contains("chao"));
    }

    /**
     * Test of initialize method, of class Sentence.
     */
    @Test
    public void testInitialize_String_String() {
        String str1 = "hola#mundo#chao";
        s1.parse(str1, "#+");
        assertEquals(s1.string, str1);
        assertNotNull(s1.wordsMultiMap.get("hola"));
        assertNotNull(s1.wordsMultiMap.get("mundo"));
        assertNotNull(s1.wordsMultiMap.get("chao"));

        assertNotNull(s1.prefixTree.contains("hola"));
        assertNotNull(s1.prefixTree.contains("mundo"));
        assertNotNull(s1.prefixTree.contains("chao"));
    }


    /**
     * Test of compareTo method, of class Sentence.
     */
    @Test
    public void testCompareTo() {
        assertTrue(s5.compareTo(s6) == s5.string.compareTo(s6.string));
    }

    @Test
    public void testMatches() {
        assertTrue(sm1.isSimilarTo(sm2));
        assertTrue(sm1.isSimilarTo(sm4));
        assertTrue(sm1.isSimilarTo(sm5));
        assertTrue(!sm1.isSimilarTo(sm3));
        assertTrue(!sm1.isSimilarTo(sm3));
        assertTrue(sm6.isSimilarTo(sm7));
        assertTrue(!sm6.isSimilarTo(sm8));
    }
}
