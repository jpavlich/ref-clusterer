/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.ucn.disc.biblio.refcluster.reference.Sentence;
import cl.ucn.disc.biblio.refcluster.reference.Word;

/**
 *
 * @author Jaime Pavlich-Mariscal
 */
public class WordTest {
    private Word word;
    private Word word2;
    private Word word3;
    private Word word4;

    public WordTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        word = new Word("a", new Sentence("aa a aa"));
        word2 = new Word("ab", new Sentence("a ab d a"));
        word3 = new Word("a", new Sentence("aa a aa"));
        word4 = new Word("a", new Sentence("qegqaa a aa"));

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addMatch method, of class Word.
     */
    @Test
    public void testAddMatch() {

        word.addMatch(word2);


        assertSame(word.matches.get(word2.sentence), word2);
        assertSame(word2.matches.get(word.sentence), word);

    }

    /**
     * Test of equals method, of class Word.
     */
    @Test
    public void testEquals() {
        assertEquals(word, word3);
        assertEquals(word, word4);
        assertFalse(word.equals(word2));
    }
}
