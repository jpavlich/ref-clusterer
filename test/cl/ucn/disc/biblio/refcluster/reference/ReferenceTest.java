/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.ucn.disc.biblio.refcluster.reference.Reference;

/**
 *
 * @author Jaime Pavlich-Mariscal
 */
public class ReferenceTest {

    private Reference r1;
    private Reference r2;
    private Reference r3;
    private Reference r4;
    private Reference r5;
    private Reference r6;

    public ReferenceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws ParseException {
        r1 = new Reference("LUBINSKI A,1999,P IFIP WG 11 3 12 IN,P223");
        r2 = new Reference("AHN GJ,2000,P 5 INT C S LOG PROG,V3,P207");
        r3 = new Reference("BELLARE M,1994,EUROCRYPT 94 PER IT,P92");
        r4 = new Reference("BENAISSA K,2003,IEEE T ELECTRON DEV,V50,P567,DOI 10.1109/TED.2003-810470");
        r5 = new Reference();
        r6 = new Reference(" AHN  GJ , 2000 , P 5  INT C S  LOG  PROG , V3 , P207 ");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getYear method, of class Reference.
     */
    @Test
    public void testGetYear() {
        assertEquals(r1.getYear(), "1999");
        assertEquals(r2.getYear(), "2000");
        assertEquals(r3.getYear(), "1994");
        assertEquals(r4.getYear(), "2003");
    }

    /**
     * Test of getAuthor method, of class Reference.
     */
    @Test
    public void testGetAuthor() {
        assertEquals(r1.getAuthor(), "LUBINSKI A");
        assertEquals(r2.getAuthor(), "AHN GJ");
        assertEquals(r3.getAuthor(), "BELLARE M");
        assertEquals(r4.getAuthor(), "BENAISSA K");


    }

    /**
     * Test of getJournal method, of class Reference.
     */
    @Test
    public void testGetJournal() {
        assertEquals(r1.getJournal(), "P IFIP WG 11 3 12 IN");
        assertEquals(r2.getJournal(), "P 5 INT C S LOG PROG");
        assertEquals(r3.getJournal(), "EUROCRYPT 94 PER IT");
        assertEquals(r4.getJournal(), "IEEE T ELECTRON DEV");

    }

    /**
     * Test of getVolume method, of class Reference.
     */
    @Test
    public void testGetVolume() {
        assertNull(r1.getVolume());
        assertEquals(r2.getVolume(), "V3");
        assertNull(r3.getVolume());
        assertEquals(r4.getVolume(), "V50");

    }

    /**
     * Test of getPage method, of class Reference.
     */
    @Test
    public void testGetPage() {
        assertEquals(r1.getPage(), "P223");
        assertEquals(r2.getPage(), "P207");
        assertEquals(r3.getPage(), "P92");
        assertEquals(r4.getPage(), "P567");

    }

    /**
     * Test of getDoi method, of class Reference.
     */
    @Test
    public void testGetDoi() {
        assertNull(r1.getDoi());
        assertNull(r2.getDoi());
        assertNull(r3.getDoi());
        assertEquals(r4.getDoi(), "DOI 10.1109/TED.2003-810470");

    }

    /**
     * Test of setRef method, of class Reference.

     * @throws InvalidReferenceException 
     */
    @Test
    public void testSetRef() throws ParseException {
        String str = "BENAISSA K,2003,IEEE T ELECTRON DEV,V50,P567,DOI 10.1109/TED.2003-810470";
        r5.parse(str);
        assertEquals(r4, r5);
        assertEquals(r4.getUnprocessedString(), r5.getUnprocessedString());
        assertEquals(r5.getString(), r5.prepareRefString(str));
        assertEquals(r5.getDoi(), "DOI 10.1109/TED.2003-810470");
        assertEquals(r5.getPage(), "P567");
        assertEquals(r5.getVolume(), "V50");
        assertEquals(r5.getJournal(), "IEEE T ELECTRON DEV");
        assertEquals(r5.getAuthor(), "BENAISSA K");
        assertEquals(r5.getYear(), "2003");
    }

    /**
     * Test of hashCode method, of class Reference.
     */
    @Test
    public void testHashCode() {
        assertEquals(r2.hashCode(), r6.hashCode());
        assertTrue(r1.hashCode() != r2.hashCode());
        assertTrue(r1.hashCode() != r3.hashCode());
        assertTrue(r1.hashCode() != r4.hashCode());
        assertTrue(r1.hashCode() != r5.hashCode());

        assertTrue(r2.hashCode() != r3.hashCode());
        assertTrue(r2.hashCode() != r4.hashCode());
        assertTrue(r2.hashCode() != r5.hashCode());

        assertTrue(r3.hashCode() != r4.hashCode());
        assertTrue(r3.hashCode() != r5.hashCode());
        assertTrue(r3.hashCode() != r6.hashCode());

        assertTrue(r4.hashCode() != r5.hashCode());
        assertTrue(r4.hashCode() != r6.hashCode());

    }

    /**
     * Test of equals method, of class Reference.
     */
    @Test
    public void testEquals() {
        assertTrue(r2.equals(r6));
        assertTrue(!r1.equals(r2));
        assertTrue(!r1.equals(r3));
        assertTrue(!r1.equals(r4));
        assertTrue(!r1.equals(r5));

        assertTrue(!r2.equals(r3));
        assertTrue(!r2.equals(r4));
        assertTrue(!r2.equals(r5));

        assertTrue(!r3.equals(r4));
        assertTrue(!r3.equals(r5));
        assertTrue(!r3.equals(r6));

        assertTrue(!r4.equals(r5));
        assertTrue(!r4.equals(r6));

    }

    /**
     * Test of compareTo method, of class Reference.
     */
    @Test
    public void testCompareTo() {
        assertEquals(r1.compareTo(r2), r1.getString().compareTo(r2.getString()));
        assertEquals(r1.compareTo(r3), r1.getString().compareTo(r3.getString()));
        assertEquals(r1.compareTo(r4), r1.getString().compareTo(r4.getString()));

        assertEquals(r1.compareTo(r6), r1.getString().compareTo(r6.getString()));

        assertEquals(r2.compareTo(r3), r2.getString().compareTo(r3.getString()));
        assertEquals(r2.compareTo(r4), r2.getString().compareTo(r4.getString()));
        assertEquals(r2.compareTo(r6), r2.getString().compareTo(r6.getString()));

        assertEquals(r3.compareTo(r4), r3.getString().compareTo(r4.getString()));

        assertEquals(r3.compareTo(r6), r3.getString().compareTo(r6.getString()));

        assertEquals(r4.compareTo(r6), r4.getString().compareTo(r6.getString()));


        try {
            r1.compareTo(r5);
        } catch (NullPointerException e) {
            assertTrue("Null pointer expected", true);
        }

        try {
            r3.compareTo(r5);
        } catch (NullPointerException e) {
            assertTrue("Null pointer expected", true);
        }

        try {
            r4.compareTo(r5);
        } catch (NullPointerException e) {
            assertTrue("Null pointer expected", true);
        }

        try {
            r2.compareTo(r5);
        } catch (NullPointerException e) {
            assertTrue("Null pointer expected", true);
        }
        
        try {
            r5.compareTo(r6);
        } catch (NullPointerException e) {
            assertTrue("Null pointer expected", true);
        }


    }

    @Test
    public void testPrepareRefString() throws ParseException {
        String origStr = "SANDHU RS, 1996, IEEE COMPUT, V29, P38, DOI 10.1109/2.485845";
        Reference r = new Reference();
        r.parse(origStr);
        String str = r.getAuthor() + "," + r.getYear() + "," + r.getJournal() + "," + r.getVolume() + "," + r.getPage() + "," + r.getDoi();
        assertTrue(r.getString().equals(str));
    }
}
