package cl.ucn.disc.biblio.refcluster;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import cl.ucn.disc.biblio.refcluster.clustering.ClustererTest;
import cl.ucn.disc.biblio.refcluster.reference.ReferenceManagerTest;
import cl.ucn.disc.biblio.refcluster.reference.ReferenceTest;
import cl.ucn.disc.biblio.refcluster.reference.SentenceTest;
import cl.ucn.disc.biblio.refcluster.reference.WordTest;

@RunWith(Suite.class)
@SuiteClasses({ClustererTest.class, ReferenceManagerTest.class, ReferenceTest.class, SentenceTest.class, WordTest.class})
public class RefClustererAllTests {

}
