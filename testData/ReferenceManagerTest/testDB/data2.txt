FN ISI Export Format
VR 1.0
PT J
AU Broustis, I
   Krishnamurthy, SV
   Faloutsos, M
   Molle, M
   Foerster, JR
AF Broustis, Ioannis
   Krishnamurthy, Srikanth V.
   Faloutsos, Michalis
   Molle, Mart
   Foerster, Jeffrey R.
TI Multiband media access control in impulse-based UWB ad hoc networks
SO IEEE TRANSACTIONS ON MOBILE COMPUTING
LA English
DT Article
DE UWB; short-range communications; medium access control; ad hoc networks
ID RADIO
AB We propose a MAC protocol for use in multihop wireless networks that
   deploy an underlying UWB ( Ultra Wide Band)-based physical layer. We
   consider a multiband approach to better utilize the available spectrum,
   where each transmitter sends longer pulses in one of many narrower
   frequency bands. The motivation comes from the observation that, in the
   absence of a sophisticated equalizer, the size of a slot for
   transmitting a UWB pulse is typically dictated by the delay spread of
   the channel. Therefore, using a wider frequency band to shorten the
   transmission time for each pulse does not increase the data rate in
   proportion to the available bandwidth. Our approach allows data
   transmissions to be contiguous and practically interference free, and,
   thus, highly efficient. For practicality, we ensure the conformance of
   our approach to FCC-imposed emission limits. We evaluate our approach
   via extensive simulations, and our results demonstrate the significant
   advantages of our approach over single-band solutions: The throughput
   increases significantly and the number of collisions decreases
   considerably. Finally, we analyze the behavior of our MAC protocol in a
   single-hop setting in terms of its efficiency in utilizing the multiple
   bands.
C1 Univ Calif Riverside, Dept Comp Sci & Engn, Riverside, CA 92521 USA.
   Intel Corp, Architecture Labs, Hillsboro, OR 97124 USA.
RP Broustis, I, Univ Calif Riverside, Dept Comp Sci & Engn, BU2,Room 351,
   Riverside, CA 92521 USA.
EM broustis@cs.ucr.edu
   krish@cs.ucr.edu
   michalis@cs.ucr.edu
   mart@cs.ucr.edu
   jeffrey.r.foerster@intel.com
CR 2004, MULTIBAND OFDM PHYS
   *ANSI IEEE, 1999, 80211 ANSIIEEE
   *DISCR TIM COMM, 2003, IEEE 802 15 3A 480MB
   *IEEE, 2003, 802153 IEEE MAC STAN
   *TX INSTR, 2003, MULT OFDM PHYS LAY P
   AGUSUT NJ, 2004, P IEEE COMM SOC C SE
   BROUSTIS I, 2005, P IEEE COMM SOC C SE
   CROVELLA ME, 1997, IEEE ACM T NETWORKIN
   CUOMO F, 2002, IEEE J SELECTED  DEC
   CUOMO F, 2002, P MOB COMM SUMM
   DING J, 2002, P SPIE ITCOM
   FOERSTER J, 2001, INTEL TECHNOLOGY J
   GALLAGER RG, 1978, P AFOSR WORKSH COMM, P74
   GARCIALUNAACEVE.JJ, 1997, P IEEE MIL COMM C MI
   GHAVAMI M, 2004, ULTRA WIDEBAND SIGNA
   GITLIN R, 1992, DATA COMMUNICATION P
   HICHAM A, 2003, ULTRAS WIDEBAND  JUN
   KLEINROCK L, 1975, QUEUEING SYSTEMS, V1
   KLEINROCK L, 1976, QUEUEING SYSTEMS, V2
   KOLENCHERY S, 1998, P IEEE MIL COMM C MI
   KRISHNAMURTHY S, 2003, INTERNETWORKING COMP
   LEBOUDEC JY, 2004, BROADNETS
   LEBOUDEC JY, 2004, IC200402 EC POL FED
   MEIER L, 2004, P ACM MOBIHOC
   MERZ R, 2004, AD HOC NOW
   PAHLAVAN K, 2002, PRINCIPLES WIRELESS
   PROAKIS JG, 2001, DIGITAL COMMUNICATIO
   RADUNOVIC B, 2004, IEEE J SELECTED AREA, V22
   ROMER K, 2001, P ACM MOBIHOC
   ROY S, 2004, P IEEE FEB
   SO J, 2004, P ACM MOBIHOC
   SOUSA ES, 1988, IEEE T COMMUN, V36, P272
   WIN MZ, 1998, IEEE COMMUN LETT, V2, P36
   WIN MZ, 2000, IEEE T COMMUN, V48, P679
   YOMO H, 2003, ELECT T ARTIFIC SEP
NR 35
TC 1
PU IEEE COMPUTER SOC
PI LOS ALAMITOS
PA 10662 LOS VAQUEROS CIRCLE, PO BOX 3014, LOS ALAMITOS, CA 90720-1314 USA
SN 1536-1233
J9 IEEE TRANS MOB COMPUT
JI IEEE. Trans. Mob. Comput.
PD APR
PY 2007
VL 6
IS 4
BP 351
EP 366
PG 16
SC Computer Science, Information Systems; Telecommunications
GA 136UV
UT ISI:000244250500002
ER

PT J
AU Fallahi, A
   Hossain, E
AF Fallahi, Afshin
   Hossain, Ekram
TI Distributed and energy-aware MAC for differentiated services wireless
   packet networks: A general queuing analytical framework
SO IEEE TRANSACTIONS ON MOBILE COMPUTING
LA English
DT Article
DE wireless packet networks; differentiated services; quality of service
   (QoS); energy efficiency; priority queuing; Markovian arrival process;
   phase-type distribution; matrix-geometric method
ID SENSOR NETWORKS; DISCRETE-TIME; PROTOCOL
AB We present a novel queuing analytical framework for the performance
   evaluation of a distributed and energy-aware medium access control
   (MAC) protocol for wireless packet data networks with service
   differentiation. Specifically, we consider a node (both buffer-limited
   and energy-limited) in the network with two different types of traffic,
   namely, high-priority and low-priority traffic, and model the node as a
   MAP (Markovian Arrival Process)/PH (Phase-Type)/1/K nonpreemptive
   priority queue. The MAC layer in the node is modeled as a server and a
   vacation queuing model is used to model the sleep and wakeup mechanism
   of the server. We study standard exhaustive and number-limited
   exhaustive vacation models both in multiple vacation case. A setup time
   for the head-of-line packet in the queue is considered, which abstracts
   the contention and the back-off mechanism of the MAC protocol in the
   node. A nonideal wireless channel model is also considered, which
   enables us to investigate the effects of packet transmission errors on
   the performance behavior of the system. After obtaining the stationary
   distribution of the system using the matrix-geometric method, we study
   the performance indices, such as packet dropping probability, access
   delay, and queue length distribution, for high-priority packets as well
   as the energy saving factor at the node. Taking into account the bursty
   traffic arrival (modeled as MAP) and, therefore, the nonsaturation case
   for the queuing analysis of the MAC protocol, using phase-type
   distribution for both the service and the vacation processes, and
   combining the priority queuing model with the vacation queuing model
   make the analysis very general and comprehensive. Typical numerical
   results obtained from the analytical model are presented and validated
   by extensive simulations. Also, we show how the optimal MAC parameters
   can be obtained by using numerical optimization.
C1 Univ Manitoba, Dept Elect & Comp Engn, Winnipeg, MB R3T 5V6, Canada.
RP Fallahi, A, Univ Manitoba, Dept Elect & Comp Engn, Winnipeg, MB R3T
   5V6, Canada.
EM afallahi@ee.umanitoba.ca
   ekram@ee.umanitoba.ca
CR *ANSI IEEE, 1999, 8802111999E ISOI 11
   AKAR N, 1998, IEEE J SELECTED AREA, V16
   ALFA AS, 1996, EUR J OPER RES, V88, P599
   ALFA AS, 1998, NAV RES LOG, V45, P23
   ALFA AS, 2003, QUEUEING SYST, V44, P5
   BARRY M, P IEEE INFOCOM 01, P582
   BHARGAHAVAN V, 1998, P IEEE INT COMP PERF
   BIANCHI G, 2000, IEEE J SELECTED AREA, V18
   BLOCK FJ, 2001, P IEEE MIL COMM C MI
   BRUNO R, 2005, IEEE COMMUN MAG, V43, P123
   CHEN D, 2004, P INT C WIR NETW ICW
   CHIASSERINI CF, 2004, P IEEE INFOCOM 04
   CHONG EKP, 2001, INTRO OPTIMIZATION
   FOH CH, 2002, P EUR WIR C
   GAO JB, 2003, IEEE T VEH TECHNOL, V52, P693, DOI 10.1109/TVT.2003.811529
   HOOKE R, 1961, J ASSOC COMPUT MACH, V8, P212
   HORVATH A, 2002, P INT C DEP SYST NET
   JONES CE, 2001, WIREL NETW, V7, P343
   KIM JH, 1999, WIRELESS PERS COMMUN, V11, P161
   MILLER MJ, 2005, IEEE T MOBILE COMPUT, V4, P228
   MINI AF, 2002, P 4 WORKSH COM SEM F
   MISIC J, 2005, AD HOC SENSOR WIRLES, V1
   NEUTS MF, 1979, J APPL PROBAB, V16, P764
   NEUTS MF, 1981, MATRIX GEOMETRIC SOL
   RAKSHIT S, 2005, IEEE T COMPUT, V54, P1384
   RHOMDANI L, 2002, RR4544 INRIA
   SCHURGERS C, 2002, IEEE T MOBILE COMPUT, V1, P70
   SINGH S, 1998, ACM COMPUTER COM JUL, P5
   SINHA A, 2001, IEEE DES TEST COMPUT, V18, P62
   TICKOO O, 2004, P IEEE INFOCOM 04 MA
   TOUMPIS S, 2003, P IEEE INT C COMM IC
   VORNEFELD U, 2002, P EUR WIR C
   WINANDS EMM, 2004, P EUR WIR C
   XIAOMING C, 2002, P IEEE 35 ANN SIM S
   YANG X, 2004, P IEEE REAL TIM EMB
   YE W, 2004, IEEE ACM T NETWORK, V12, P493, DOI 10.1109/TNET.2004.828953
   YEH CH, 2003, P 8 IEEE INT S COMP
   YOUNIS M, 2004, P 37 HAW INT C SYST
   YU J, 2004, P IEEE INT C COMM IC
   ZIOUVA E, 2002, COMPUT COMMUN, V25, P313
NR 40
TC 5
PU IEEE COMPUTER SOC
PI LOS ALAMITOS
PA 10662 LOS VAQUEROS CIRCLE, PO BOX 3014, LOS ALAMITOS, CA 90720-1314 USA
SN 1536-1233
J9 IEEE TRANS MOB COMPUT
JI IEEE. Trans. Mob. Comput.
PD APR
PY 2007
VL 6
IS 4
BP 381
EP 394
PG 14
SC Computer Science, Information Systems; Telecommunications
GA 136UV
UT ISI:000244250500004
ER

PT J
AU Chang, BJ
   Chou, CM
   Liang, YH
AF Chang, Ben-Jye
   Chou, Chien-Ming
   Liang, Ying-Hsin
TI Markov chain analysis of uplink subframe in polling-based WiMAX networks
SO COMPUTER COMMUNICATIONS
LA English
DT Article
DE WiMAX; Markov chain; performance analysis; number of polls; polling
   delay
ID BAND WIRELESS NETWORKS; SERVICE; SUPPORT
AB IEEE 802.16 is proposed to provide middle-range access and high data
   rate for real-time transmission through wireless channel. The
   increasing of access range, data rate and total number of nodes makes
   the wireless media access more difficult and critical. To overcome
   access contentions from requests, IEEE 802.16 thus adopts a
   centralized-based polling MAC instead of random access control. In such
   a polling mechanism, the number of polls (NPS) within a frame period
   becomes an important factor. There are two main reasons. First, not all
   of the polled nodes have packets to be sent, and thus wastes bandwidth
   used for polling these nodes. Second, that the node has packets to be
   sent may not be polled immediately, and then increases polling delay.
   Therefore, this paper analyzes NPS based on the Markov chain analysis
   to minimize average polling delay while increasing network throughput.
   Numerical results indicate that the analysis results are very close to
   the simulation results. Moreover, the number of polls and the waste
   rate are also analyzed under various NDS and offered loads. (C) 2008
   Elsevier B.V. All rights reserved.
C1 [Chang, Ben-Jye] Chaoyang Univ Technol, Dept Comp Sci & Informat Engn, Taichung, Taiwan.
   [Chou, Chien-Ming] Chaoyang Univ Technol, Grad Inst Networking & Commun Engn, Taichung, Taiwan.
RP Chang, BJ, Chaoyang Univ Technol, Dept Comp Sci & Informat Engn,
   Taichung, Taiwan.
EM changb@mail.cyut.edu.tw
   s9430612@mail.cyut.edu.tw
   t136@nkc.edu.tw
CR *GNU, GNU COMP COLL GCC
   *IEEE, 2006, 80216E IEEE 16
   CHANG BJ, 2003, INFORM SCIENCES, V151, P1, DOI
   10.1016/S0020-0255(02)00274-8
   CHANG BJ, 2004, IEEE ACM T NETWORK, V12, P187, DOI
   10.1109/TNET.2003.822642
   CHANG BJ, 2006, IEEE ICCS2006, P1
   CHANG BJ, 2007, IEEE WCNC2007, P2757
   CHEN J, 2005, IEEE P ICC 05 MAY, V5, P3422
   CHO DH, 2005, P 1 INT C DISTR FRAM, P130
   CHU GS, 2002, IEEE INT C COMM CIRC, V1, P435
   CICCONETTI C, 2006, IEEE NETWORK, V20, P50
   CICCONETTI C, 2007, IEEE T MOBILE COMPUT, V6, P26
   GAKHAR K, 2006, IWQOS 2006, P140
   HOWARD RA, 1960, DYNAMIC PROGRAMMING
   HWANG RH, 1999, COMPUTER NETWORKS, V34, P241
   HWANG RH, 2004, 6 INT C ADV COMM TEC, V2, P949
   NIYATO D, 2006, IEEE T COMPUT, V55, P1473
   NIYATO D, 2006, IEEE T MOBILE COMPUT, V5, P668
   OH SM, 2005, IEEE PERV COMP COMM, P215
   OH SM, 2005, P 3 IEEE INT C PERV, P215
   WANG H, 2005, WIR TEL S 2005 APR, P60
   WONGTHAVARAWAT K, 2003, IEEE MIL COMM C OCT, V2, P779
   YAO Y, 2005, STUDY UGS GRANT SYNC, P105
NR 22
TC 5
PU ELSEVIER SCIENCE BV
PI AMSTERDAM
PA PO BOX 211, 1000 AE AMSTERDAM, NETHERLANDS
SN 0140-3664
J9 COMPUT COMMUN
JI Comput. Commun.
PD JUN 25
PY 2008
VL 31
IS 10
BP 2381
EP 2392
DI 10.1016/j.comcom.2008.03.004
PG 12
SC Computer Science, Information Systems; Engineering, Electrical &
   Electronic; Telecommunications
GA 331TQ
UT ISI:000258035900048
ER

EF