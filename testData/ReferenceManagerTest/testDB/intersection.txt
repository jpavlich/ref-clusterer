FN ISI Export Format
VR 1.0
PT J
AU Chang, BJ
   Chou, CM
   Liang, YH
AF Chang, Ben-Jye
   Chou, Chien-Ming
   Liang, Ying-Hsin
TI Markov chain analysis of uplink subframe in polling-based WiMAX networks
SO COMPUTER COMMUNICATIONS
LA English
DT Article
DE WiMAX; Markov chain; performance analysis; number of polls; polling
   delay
ID BAND WIRELESS NETWORKS; SERVICE; SUPPORT
AB IEEE 802.16 is proposed to provide middle-range access and high data
   rate for real-time transmission through wireless channel. The
   increasing of access range, data rate and total number of nodes makes
   the wireless media access more difficult and critical. To overcome
   access contentions from requests, IEEE 802.16 thus adopts a
   centralized-based polling MAC instead of random access control. In such
   a polling mechanism, the number of polls (NPS) within a frame period
   becomes an important factor. There are two main reasons. First, not all
   of the polled nodes have packets to be sent, and thus wastes bandwidth
   used for polling these nodes. Second, that the node has packets to be
   sent may not be polled immediately, and then increases polling delay.
   Therefore, this paper analyzes NPS based on the Markov chain analysis
   to minimize average polling delay while increasing network throughput.
   Numerical results indicate that the analysis results are very close to
   the simulation results. Moreover, the number of polls and the waste
   rate are also analyzed under various NDS and offered loads. (C) 2008
   Elsevier B.V. All rights reserved.
C1 [Chang, Ben-Jye] Chaoyang Univ Technol, Dept Comp Sci & Informat Engn, Taichung, Taiwan.
   [Chou, Chien-Ming] Chaoyang Univ Technol, Grad Inst Networking & Commun Engn, Taichung, Taiwan.
RP Chang, BJ, Chaoyang Univ Technol, Dept Comp Sci & Informat Engn,
   Taichung, Taiwan.
EM changb@mail.cyut.edu.tw
   s9430612@mail.cyut.edu.tw
   t136@nkc.edu.tw
CR *GNU, GNU COMP COLL GCC
   *IEEE, 2006, 80216E IEEE 16
   CHANG BJ, 2003, INFORM SCIENCES, V151, P1, DOI    10.1016/S0020-0255(02)00274-8
   CHANG BJ, 2004, IEEE ACM T NETWORK, V12, P187, DOI    10.1109/TNET.2003.822642
   CHANG BJ, 2006, IEEE ICCS2006, P1
   CHANG BJ, 2007, IEEE WCNC2007, P2757
   CHEN J, 2005, IEEE P ICC 05 MAY, V5, P3422
   CHO DH, 2005, P 1 INT C DISTR FRAM, P130
   CHU GS, 2002, IEEE INT C COMM CIRC, V1, P435
   CICCONETTI C, 2006, IEEE NETWORK, V20, P50
   CICCONETTI C, 2007, IEEE T MOBILE COMPUT, V6, P26
   GAKHAR K, 2006, IWQOS 2006, P140
   HOWARD RA, 1960, DYNAMIC PROGRAMMING
   HWANG RH, 1999, COMPUTER NETWORKS, V34, P241
   HWANG RH, 2004, 6 INT C ADV COMM TEC, V2, P949
   NIYATO D, 2006, IEEE T COMPUT, V55, P1473
   NIYATO D, 2006, IEEE T MOBILE COMPUT, V5, P668
   OH SM, 2005, IEEE PERV COMP COMM, P215
   OH SM, 2005, P 3 IEEE INT C PERV, P215
   WANG H, 2005, WIR TEL S 2005 APR, P60
   WONGTHAVARAWAT K, 2003, IEEE MIL COMM C OCT, V2, P779
   YAO Y, 2005, STUDY UGS GRANT SYNC, P105
NR 22
TC 5
PU ELSEVIER SCIENCE BV
PI AMSTERDAM
PA PO BOX 211, 1000 AE AMSTERDAM, NETHERLANDS
SN 0140-3664
J9 COMPUT COMMUN
JI Comput. Commun.
PD JUN 25
PY 2008
VL 31
IS 10
BP 2381
EP 2392
DI 10.1016/j.comcom.2008.03.004
PG 12
SC Computer Science, Information Systems; Engineering, Electrical &
   Electronic; Telecommunications
GA 331TQ
UT ISI:000258035900048
ER

