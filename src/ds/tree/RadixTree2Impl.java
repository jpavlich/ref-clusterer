/*
The MIT License

Copyright (c) 2008 Tahseen Ur Rehman, Javid Jamae

http://code.google.com/p/radixtree/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

package ds.tree;

import java.util.LinkedHashSet;
import java.util.Set;


public class RadixTree2Impl<T> extends RadixTreeImpl<T> implements RadixTree2<T> {
	public Set<T> getLeafValues(String key) {
		Set<T> leafValues = new LinkedHashSet<T>();
        Visitor<T,RadixTreeNode<T>> visitor = new VisitorImpl<T,RadixTreeNode<T>>() {

            public void visit(String key, RadixTreeNode<T> parent,
                    RadixTreeNode<T> node) {
                if (node.isReal()) 
                    result = node;
            }
        };
        
        visit(key, visitor);
        
        RadixTreeNode<T> node = visitor.getResult();
        
        if (node != null) {
        	leafValues.addAll(getLeafValues(key, node));
        }
        
        return leafValues;
	}

	protected Set<T> getLeafValues(String key, RadixTreeNode<T> node) {
		Set<T> leafValues = new LinkedHashSet<T>();
		if (node.isReal() && node.getChildern().size() == 0) { // node is a leaf
			leafValues.add(node.getValue());
		}
		for (RadixTreeNode<T> child : node.getChildern()) {
			leafValues.addAll(getLeafValues(key,child));
		}
		return leafValues;
	}
	
	public boolean hasSuperstrings(String key) {
        Visitor<T,RadixTreeNode<T>> visitor = new VisitorImpl<T,RadixTreeNode<T>>() {

            public void visit(String key, RadixTreeNode<T> parent,
                    RadixTreeNode<T> node) {
                if (node.isReal()) 
                    result = node;
            }
        };
        
        visit(key, visitor);
        
        RadixTreeNode<T> node = visitor.getResult();
        return node != null;
	}
}
