package ds.tree;

import java.util.Set;

public interface RadixTree2<T> extends RadixTree<T> {

	public Set<T> getLeafValues(String key);

	public boolean hasSuperstrings(String key);

}