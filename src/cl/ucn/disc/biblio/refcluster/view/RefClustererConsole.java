package cl.ucn.disc.biblio.refcluster.view;

import groovy.ui.Console;
import cl.ucn.disc.biblio.refcluster.control.ReferenceClustererConsoleCommands;

public class RefClustererConsole {
	public static void main(String[] args) {
		Console console = new Console();
		console.setVariable("cmd", new ReferenceClustererConsoleCommands());
		console.run();
	}
}
