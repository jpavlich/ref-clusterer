package cl.ucn.disc.biblio.refcluster.control;

import java.io.FileNotFoundException;

import cl.ucn.disc.biblio.refcluster.reference.ReferenceManager;

public class ReferenceClustererConsoleCommands {

	private ReferenceManager rm = new ReferenceManager();

	public synchronized void difference(String outputFile, String wosFile1, String wosFile2) throws FileNotFoundException {
		rm.difference(outputFile, wosFile1, wosFile2);
	}

	public synchronized void intersection(String outputFile, String... inputFiles) throws FileNotFoundException {
		rm.intersection(outputFile, inputFiles);
	}

	public synchronized void union(String outputFile, String... inputFiles) throws FileNotFoundException {
		rm.union(outputFile, inputFiles);
	}



}
