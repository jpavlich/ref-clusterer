/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.util;

/** Provides a simple interface for classes that transform strings.
 * @author Jaime Pavlich-Mariscal
 *
 */
public interface TextTransformer {
	
	/** Transforms a string into another
	 * @param string The input string
	 * @return The transformed string
	 * @throws TextTransformException If the transformation could not be performed successfully.
	 */
	public String transform(String string) throws TextTransformException;
}
