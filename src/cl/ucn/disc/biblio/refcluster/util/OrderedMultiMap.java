/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package cl.ucn.disc.biblio.refcluster.util;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class OrderedMultiMap<T1, T2> extends MultiMap<T1, T2> {

	@Override
	protected Map<T1, Set<T2>> getMap() {
		return new TreeMap<T1,Set<T2>>();
	}


}
