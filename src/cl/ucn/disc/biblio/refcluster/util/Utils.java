/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.util;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import cl.ucn.disc.biblio.refcluster.database.TupleOfStrings;

/**
 * Miscelaneous utility methods
 * 
 * @author Jaime Pavlich-Mariscal
 * 
 */
public class Utils {

	/**
	 * Creates a set comprised by the projection of a field over a set of elements. Creates a list
	 * of sentences. Each sentence is created from a set of Strings obtained from a specified field
	 * of each object in a set.
	 * 
	 * @param set
	 *            The set of objects whose specified fields will be used to construct the projection
	 * @param field
	 *            The field that will be projected over {@code set}.
	 * @param collectionClass The class of the collection to create as a result of the projection.
	 * @return The list of sentences, where each sentence is constructed from the string obtained
	 *         from the {@code field} of each object in {@code set}
	 */
	public static <C extends Collection<T>, T extends TupleOfStrings> C projection(Set<?> set, Field field, Class<C> collectionClass, Class<T> tupleClass) {

		try {
			field.setAccessible(true);
			C result = collectionClass.newInstance();
			for (Object obj : set) {
				try {
					T tuple = tupleClass.newInstance();
					tuple.parse(String.valueOf(field.get(obj)));
					result.add(tuple);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	@SuppressWarnings("unchecked")
	public static <T> Set<T> projection(Set<?> set, String fieldName, Class<T> tupleClass) {

		try {
			
			Set <T> result = new LinkedHashSet<T>();
			for (Object obj : set) {
				try {
					Field field = obj.getClass().getDeclaredField(fieldName);
					field.setAccessible(true);
					T value = (T) field.get(obj);
					if (value != null) {
						result.add(value);
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public static String getLongest(Collection<String> strings) {
		if (strings.isEmpty()) {
			return null;
		}
		String longest = "";
		for (Iterator<String> it = strings.iterator(); it.hasNext(); ) {
			String t = it.next();
			if (t.length() > longest.length()) {
				longest = t;
			}
		}
		return longest;

	}

	/**
	 * Deletes all files in a folder
	 * 
	 * @param folder
	 */
	public static void deleteFilesInFolder(File folder) {
		for (File f : folder.listFiles()) {
			f.delete();
		}
	}

}
