/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.ucn.disc.biblio.refcluster.reference;

import java.util.HashMap;
import java.util.Map;

/** Represents a word in a sentence
 * @see Sentence
 * @author Jaime Pavlich-Mariscal
 *
 */
public class Word { // Do not implement Comparable! 
	String string;
	Sentence sentence;
	Map<Sentence,Word> matches = new HashMap<Sentence,Word>();
	
	public Word(String string, Sentence sentence) {
		super();
		this.string = string;
		this.sentence = sentence;
	}

	
	
	@Override
	public String toString() {
		return "Word [" + (string != null ? "string=" + string : "") + "]";
	}

	public void addMatch(Word e) {
		matches.put(e.sentence,e);
		e.matches.put(sentence, this);
	}

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Word other = (Word) obj;
        if ((this.string == null) ? (other.string != null) : !this.string.equals(other.string)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.string != null ? this.string.hashCode() : 0);
        return hash;
    }






}
