/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.ucn.disc.biblio.refcluster.database;

import java.text.ParseException;

import cl.ucn.disc.biblio.refcluster.reference.Reference;
import cl.ucn.disc.biblio.refcluster.reference.Sentence;


/** Represents tuples of strings. This is used to provide a common interface
 * for classes {@link Reference} and {@link Sentence}.
 *
 * @author Jaime Pavlich-Mariscal
 */
public interface TupleOfStrings<T> extends Comparable <T> {
	/** Populates this tuple of strings with the tokens obtained from parsing a string {@code str}
	 * @param str The string to parse
	 * @throws ParseException 
	 */
	public void parse(String str) throws ParseException;

	
	/** Obtains the string that was used to construct this tuple.
	 * The string may have been formatted, e.g., it may have trimmed spaces
	 * @return
	 */
	public abstract String getString();
	
	/** Obtains the raw string used to construct this tuple. This string 
	 * must be identical to the one used to construct this tuple (the parameter of {@link TupleOfStrings#parse(String)})
	 * @return
	 */
	public String getUnprocessedString();
	
	

}
