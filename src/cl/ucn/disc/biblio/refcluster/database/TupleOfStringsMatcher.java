/*
 *  Copyright (C) 2011 Jaime Pavlich-Mariscal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cl.ucn.disc.biblio.refcluster.database;

import cl.ucn.disc.biblio.refcluster.clustering.ClusterManager;
import cl.ucn.disc.biblio.refcluster.reference.Reference;
import cl.ucn.disc.biblio.refcluster.reference.Sentence;

/** Provides an interface for classes that match tuples of strings. It is used intensively by the
 * clustering algorithms in {@link ClusterManager}
 * @see ClusterManager
 * @see Reference#JOURNAL_VOL_PAGE_MATCHER
 * @see Sentence#SIMILAR_SENTENCE_MATCHER
 * 
 * @author Jaime Pavlich-Mariscal
 *
 * @param <T>
 */
public abstract class TupleOfStringsMatcher<T extends TupleOfStrings> {
    public abstract boolean matches(T o1, T o2);

    
}
